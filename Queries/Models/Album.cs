﻿using System.Collections.Generic;

namespace Queries.Models {
	public class Album {
		public int Id { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public int Year { get; set; }
		public string Picture { get; set; }
		public virtual List<Song> Songs { get; set; }
		public int ArtistId { get; set; }
		public virtual Artist Artist { get; set; }
	}
}
