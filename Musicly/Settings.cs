﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Musicly
{
    public class Settings
    {
        private IConfiguration configuration;
        public Settings(IConfiguration _configuration)
        {
            configuration = _configuration;
        }
        public string TokenKey => configuration.GetValue<string>("TokenKey");
        public string IdentityServiceURL => configuration.GetValue<string>("IdentityServiceURL");
    }
}
