﻿using System.Collections.Generic;

namespace Queries.Models {
	public class Artist {
		public int Id { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public int StartYear { get; set; }
		public int EndYear { get; set; }
		public string Picture { get; set; }

		public virtual List<Album> Albums { get; set; }

	}
}
