﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Musicly.Models;

namespace Musicly.Controllers {
	public class HomeController : BaseController {
		private readonly ApplicationContext db = new ApplicationContext();

		public IActionResult Index() {
			var songs = this.db.Songs.ToList();
			return View(songs);
		}

		[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
		public IActionResult Error() {
			return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
		}
	}
}
