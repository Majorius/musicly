﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Musicly.Models;

namespace Musicly.Controllers
{
    [AllowAnonymous]
    public class LoginController : Controller
    {
        private readonly Settings settings;
        public LoginController(Settings _settings)
        {
            settings = _settings;
        }
        public IActionResult Index()
        {
            return View(settings);
        }
        public IActionResult Logout()
        {
            return View(settings);
        }
    }
}