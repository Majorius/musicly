﻿namespace Queries.Models {
	public class Song {
		public int Id { get; set; }
		public string Name { get; set; }
		public string BlobId { get; set; }
		public int AlbumId { get; set; }
		public virtual Album Album { get; set; }
	}
}
