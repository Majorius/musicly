﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Queries.Migrations
{
    public partial class SoldiersBlobIdAdd : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Songs",
                keyColumn: "Id",
                keyValue: 1,
                column: "BlobId",
                value: "1bdc65e4-a2a2-497f-884a-d79da6b7cc6f");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Songs",
                keyColumn: "Id",
                keyValue: 1,
                column: "BlobId",
                value: null);
        }
    }
}
