﻿using Microsoft.EntityFrameworkCore;
using Queries.Models;

namespace Musicly {
	public class ApplicationContext : DbContext {
		public DbSet<Album> Albums { get; set; }
		public DbSet<Song> Songs { get; set; }
		public DbSet<Artist> Artists { get; set; }

		public ApplicationContext() { }

		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) {
			optionsBuilder
				.UseLazyLoadingProxies()
				.UseSqlServer(@"Server=tcp:musicly-db.database.windows.net,1433;Initial Catalog=musicly-db-main;
				Persist Security Info=False;User ID=musiclydb_admin;Password=PMI51musicly;MultipleActiveResultSets=False;
				Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
		}

		protected override void OnModelCreating(ModelBuilder modelBuilder) {
			modelBuilder.Entity<Artist>().HasData(new Artist {
				Id = 1,
				Name = "Soldiers",
				Description = "This is iconic band",
				StartYear = 1998,
				EndYear = 2007,
				Picture = "soldiers.jpg"
			});
			modelBuilder.Entity<Album>().HasData(new Album[] {
				new Album {
					Id = 1,
					Name = "Iconic album",
					Description = "This is iconic album",
					Year = 2000,
					Picture = "soldiers.jpg",
					ArtistId = 1
				}, new Album {
					Id = 2,
					Name = "Dookie",
					Description = "Someething meaningful",
					ArtistId = 1,
					Picture = "Dookie.jpg",
					Year = 2006
				},new Album {
					Id = 3,
					Name = "The Black Parade",
					Description = "Someething meaningful",
					ArtistId = 1,
					Picture = "BlackParade.jpg",
					Year = 2007
				}
			});
			modelBuilder.Entity<Song>().HasData(new Song[] {
				new Song { Id=1, Name="Soldiers", AlbumId = 1, BlobId = "1bdc65e4-a2a2-497f-884a-d79da6b7cc6f" },
				new Song { Id=2, Name="Welcome to the Black Parade", AlbumId = 3, BlobId = "d92e931e-7206-4d0f-9f01-68fbd778e9ad" },
				new Song { Id=3, Name="Basket case", AlbumId = 2, BlobId = "b0958943-795f-419d-b9d3-4f9fd10e45b6" }
			});
		}
	}
}
